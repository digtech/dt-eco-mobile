package dt.devsquad.dttour;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.here.android.mpa.common.GeoPolyline;
import com.here.android.mpa.common.Image;
import com.here.android.mpa.mapping.MapCircle;
import com.here.android.mpa.mapping.MapMarker;
import com.here.android.mpa.mapping.MapObject;
import com.here.android.mpa.mapping.MapPolyline;

import org.jetbrains.annotations.NotNull;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.here.android.mpa.common.GeoCoordinate;
import com.here.android.mpa.common.OnEngineInitListener;
import com.here.android.mpa.mapping.Map;
import com.here.android.mpa.mapping.AndroidXMapFragment;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;


public class InfoActivity extends AppCompatActivity {

    // permissions request code
    private final static int REQUEST_CODE_ASK_PERMISSIONS = 1;

    private static final String[] REQUIRED_SDK_PERMISSIONS = new String[] {
            Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.WRITE_EXTERNAL_STORAGE };

    // map embedded in the map fragment
    private Map map = null;

    Card card;

    // map fragment embedded in this activity
    private AndroidXMapFragment mapFragment = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle arguments = getIntent().getExtras();
        if(arguments!=null){
            card = (Card) arguments.getSerializable(Card.class.getSimpleName());
        }else {
            card = new Card("pizdec","nahui","https:\\/\\/dt-tour.tk\\/assets\\/trip.jpg",new JSONObject());
            card.setUrl("https://dt-tour.tk/api/v2/Api.php?apicall=getplace&id=1");
        }
        checkPermissions();
    }

    public void onButtonLogin(View view){
        Intent intent = new Intent(InfoActivity.this, ModalActivity.class);
        intent.putExtra(Card.class.getSimpleName(), card);
        startActivity(intent);
        //startActivity(new Intent(InfoActivity.this, ModalActivity.class));
    }

    /**
     * Checks the dynamically controlled permissions and requests missing permissions from end user.
     */
    protected void checkPermissions() {
        final List<String> missingPermissions = new ArrayList<String>();
        // check all required dynamic permissions
        for (final String permission : REQUIRED_SDK_PERMISSIONS) {
            final int result = ContextCompat.checkSelfPermission(this, permission);
            if (result != PackageManager.PERMISSION_GRANTED) {
                missingPermissions.add(permission);
            }
        }
        if (!missingPermissions.isEmpty()) {
            // request all missing permissions
            final String[] permissions = missingPermissions
                    .toArray(new String[missingPermissions.size()]);
            ActivityCompat.requestPermissions(this, permissions, REQUEST_CODE_ASK_PERMISSIONS);
        } else {
            final int[] grantResults = new int[REQUIRED_SDK_PERMISSIONS.length];
            Arrays.fill(grantResults, PackageManager.PERMISSION_GRANTED);
            onRequestPermissionsResult(REQUEST_CODE_ASK_PERMISSIONS, REQUIRED_SDK_PERMISSIONS,
                    grantResults);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_PERMISSIONS:
                for (int index = permissions.length - 1; index >= 0; --index) {
                    if (grantResults[index] != PackageManager.PERMISSION_GRANTED) {
                        // exit the app if one permission is not granted
                        Toast.makeText(this, "Required permission '" + permissions[index]
                                + "' not granted, exiting", Toast.LENGTH_LONG).show();
                        finish();
                        return;
                    }
                }
                // all permissions were granted
                initialize();
                break;
        }
    }

    private void initialize() {
        setContentView(R.layout.activity_info);
        //Picasso.with(this).load(card.id).resize(1200, 1200).centerCrop().into(((ImageView) findViewById(R.id.imageUrlEnd)));
        ((TextView) findViewById(R.id.textName)).setText(card.textFirst);
        ((TextView) findViewById(R.id.textAuthor)).setText((String) card.array.get("owner"));
        ((TextView) findViewById(R.id.textPeople)).setText((String) card.array.get("people"));
        ((TextView) findViewById(R.id.textMoney)).setText(card.textSecond);
        ((TextView) findViewById(R.id.textDate)).setText((String) card.array.get("datestart"));
        ((TextView) findViewById(R.id.textMarker)).setText((String) card.array.get("place"));
        ((TextView) findViewById(R.id.textKura)).setText((String) card.array.get("owner"));
        // Search for the map fragment to finish setup by calling init().
        mapFragment = getMapFragment();

        // Set up disk cache path for the map service for this application
        boolean success = com.here.android.mpa.common.MapSettings.setIsolatedDiskCacheRootPath(
                getApplicationContext().getExternalFilesDir(null) + File.separator + ".here-maps");

        mapFragment.init(new OnEngineInitListener() {
            @Override
            public void onEngineInitializationCompleted(
                    final OnEngineInitListener.Error error) {
                if (error == OnEngineInitListener.Error.NONE) {
                    // retrieve a reference of the map from the map fragment
                    map = mapFragment.getMap();
                    // Set the zoom level to the average between min and max
                    map.setZoomLevel((map.getMaxZoomLevel() + map.getMinZoomLevel()) / 2.2);

//                        hereMap)
                        List<MapObject> testCircle = new ArrayList<MapObject>();
                        testCircle.add(new MapCircle(2500,new GeoCoordinate(59.74312,30.38925,12)));
                        testCircle.add(new MapCircle(2500,new GeoCoordinate(59.67766,30.34668,12)));
                        testCircle.add(new MapCircle(2500,new GeoCoordinate(59.67766,30.42109,12)));
                        testCircle.add(new MapCircle(2500,new GeoCoordinate(59.72312,30.38925,12)));
                        testCircle.add(new MapCircle(2500,new GeoCoordinate(59.70312,30.38925,12)));
                        testCircle.add(new MapCircle(2500,new GeoCoordinate(59.68312,30.38925,12)));
                        map.addMapObjects(testCircle);

                    new JsonUrlReader2().execute(card.getUrl());

                } else {
                    System.out.println("ERROR: Cannot initialize Map Fragment");

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            new AlertDialog.Builder(InfoActivity.this).setMessage(
                                    "Error : " + error.name() + "\n\n" + error.getDetails())
                                    .setTitle("pizda")
                                    .setNegativeButton(android.R.string.cancel,
                                            new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(
                                                        DialogInterface dialog,
                                                        int which) {
                                                    finishAffinity();
                                                }
                                            }).create().show();
                        }
                    });
                }
            }
        });
    }

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        GeoCoordinate bmImage;

        DownloadImageTask(GeoCoordinate bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
                mIcon11 = mIcon11.createScaledBitmap(mIcon11, 150, 150, true);
            } catch (Exception e) {
                Log.e("Ошибка передачи изображения", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            createMarker(result, bmImage);
        }
    }

    void createMarker(Bitmap image, GeoCoordinate num) {
        Image images = new Image();
        images.setBitmap(image);

        map.addMapObject(new MapMarker(num, images));
    }

    private AndroidXMapFragment getMapFragment() {
        return (AndroidXMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapfragment);
    }

    private class JsonUrlReader2 extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(@NotNull String... path) {
            String content;
            try {
                content = getContent(path[0]);
            } catch (IOException ex) {
                content = ex.getMessage();
            }
            return content;
        }

        @Override
        protected void onPostExecute(String content) {
            List<GeoCoordinate> testPoint = new ArrayList<GeoCoordinate>();
            List<JSONObject> js = new ArrayList<>();
            try {

                JSONParser parser = new JSONParser();
                JSONObject a = (JSONObject) parser.parse(content);
                JSONArray b = (JSONArray) a.get("stud");

                for (Object o : b) {
                    org.json.simple.JSONObject equip = (JSONObject) o;
                    try {

                        GeoCoordinate geo = new GeoCoordinate(Double.parseDouble((String) equip.get("x")),Double.parseDouble((String) equip.get("y")),0.0);
                        new DownloadImageTask(geo).execute((String) equip.get("img"));
                        testPoint.add(geo);
                        js.add(equip);

                    } catch (Exception e){}
                }

                MapPolyline mapPolyline = new MapPolyline(new GeoPolyline(testPoint));
                mapPolyline.setLineWidth(10);
                map.addMapObject(mapPolyline);
                map.setCenter(testPoint.get(0), Map.Animation.NONE);
                card.setObjectList(js);

            } catch (Exception e) { }
        }

        @NotNull
        private String getContent(String path) throws IOException {
            BufferedReader reader = null;
            try {
                URL url = new URL(path);
                HttpsURLConnection c = (HttpsURLConnection) url.openConnection();
                c.setRequestMethod("GET");
                c.setReadTimeout(10000);
                c.connect();
                reader = new BufferedReader(new InputStreamReader(c.getInputStream()));
                StringBuilder buf = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    buf.append(line + "\n");
                }
                return (buf.toString());
            } finally {
                if (reader != null) {
                    reader.close();
                }
            }
        }
    }
}

