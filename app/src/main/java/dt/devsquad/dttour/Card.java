package dt.devsquad.dttour;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.io.Serializable;
import java.util.List;

public class Card implements Serializable {
    String textFirst;
    String textSecond;
    //int value;
    String id;
    private String url;
    JSONObject array;
    List<JSONObject> objectList;

    Card(String textFirst, String textSecond, String id, JSONObject array) {
        this.textFirst = textFirst;
        this.textSecond = textSecond;
        //this.value = value;
        this.id = id;
        this.array = array;
    }

    public void setObjectList(List<JSONObject> objectList) {
        this.objectList = objectList;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
