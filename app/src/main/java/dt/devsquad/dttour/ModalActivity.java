package dt.devsquad.dttour;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import org.json.simple.JSONObject;

public class ModalActivity extends AppCompatActivity {

    Card card;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modal);

        Bundle arguments = getIntent().getExtras();
        final Card card;
        if(arguments!=null){
            card = (Card) arguments.getSerializable(Card.class.getSimpleName());
        }else {
            card = new Card("pizdec","nahui","blyat",new JSONObject());
            card.setUrl("https://dt-tour.tk/api/v2/Api.php?apicall=gettour&id=2");
        }

        ((TextView) findViewById(R.id.name)).setText(card.textFirst);
        ((TextView) findViewById(R.id.place)).setText((String) card.array.get("place"));
        ((TextView) findViewById(R.id.head1)).setText((String) card.objectList.get(0).get("name"));
        ((TextView) findViewById(R.id.body1)).setText((String) card.objectList.get(0).get("description"));
        ((TextView) findViewById(R.id.head2)).setText((String) card.objectList.get(1).get("name"));
        ((TextView) findViewById(R.id.body2)).setText((String) card.objectList.get(1).get("description"));
        ((TextView) findViewById(R.id.head3)).setText((String) card.objectList.get(2).get("name"));
        ((TextView) findViewById(R.id.body3)).setText((String) card.objectList.get(2).get("description"));
    }
}
